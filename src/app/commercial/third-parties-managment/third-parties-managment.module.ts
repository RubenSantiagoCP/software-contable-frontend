import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThirdPartiesManagmentRoutingModule } from './third-parties-managment-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ThirdPartiesManagmentRoutingModule
  ]
})
export class ThirdPartiesManagmentModule { }
