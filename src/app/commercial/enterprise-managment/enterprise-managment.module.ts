import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnterpriseManagmentRoutingModule } from './enterprise-managment-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EnterpriseManagmentRoutingModule
  ]
})
export class EnterpriseManagmentModule { }
