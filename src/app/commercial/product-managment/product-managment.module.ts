import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductManagmentRoutingModule } from './product-managment-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProductManagmentRoutingModule
  ]
})
export class ProductManagmentModule { }
